package drita.ccc;

import java.io.*;
import java.math.BigDecimal;

public class Fractals {

	public static void main(String[] args) throws IOException {
		
		BufferedReader br = new BufferedReader(new FileReader("inputTriangle.txt"));

		String zeile = "";
		String[] arrZeile = null;

		while ((zeile = br.readLine()) != null) {
			arrZeile = zeile.split(" ");

			String typ = arrZeile[0];
			double length = Integer.parseInt(substringAfter(arrZeile[1], "="));
			double iteration = Integer
					.parseInt(substringAfter(arrZeile[2], "="));

			if (typ.equals("triangle")) {
				System.out.println(berechneFlacheDreieck(length, iteration));

			} else if (typ.equals("sq")) {
				System.out.println(berechneFlacheViereck(length, iteration));
			}

		}

		br.close();
	}

	// ******************************************************************************************************

	// ******************************************************************************************************

	/**
	 * formel f�r umfang: seitenlange *(4/3)^iteration - nur fur eine seite f�r
	 * alle 3 seiten,muss noch mit 3 multiplicieren
	 * 
	 * @param lange
	 * @param iteratio
	 * @return
	 */
	private static double berechneFlacheDreieck(double lange, double iteratio) {

		BigDecimal nenner = new BigDecimal(
				String.valueOf(Math.pow(4, iteratio)));
		BigDecimal zahler = new BigDecimal(
				String.valueOf(Math.pow(3, iteratio)));

		BigDecimal bruch = zahler.divide(nenner);

		return bruch.multiply(new BigDecimal(String.valueOf(lange * 3)))
				.doubleValue();

	}

	// ******************************************************************************************************

	// ******************************************************************************************************

	/**
	 * formel f�r umfang: seitenlange *(5/3)^iteration - nur fur eine seite f�r
	 * alle 4 seiten,muss noch mit 3 multiplicieren
	 * 
	 * @param lange
	 * @param iteratio
	 * @return
	 */
	private static double berechneFlacheViereck(double lange, double iteratio) {

		BigDecimal nenner = new BigDecimal(
				String.valueOf(Math.pow(5, iteratio)));
		BigDecimal zahler = new BigDecimal(
				String.valueOf(Math.pow(3, iteratio)));

		BigDecimal bruch = zahler.divide(nenner);

		return bruch.multiply(new BigDecimal(String.valueOf(lange * 4)))
				.doubleValue();

	}

	// ******************************************************************************************************

	// ******************************************************************************************************

	private static String substringAfter(String string, String trennzeichen) {
		int position = string.indexOf(trennzeichen);

		return position >= 0 ? string.substring(position + trennzeichen.length()) : "";
	}
}
